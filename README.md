DoFcalc
=======

Measures for degrees of freedom in agents, and agent ensembles like virtual machines.
